<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>A04_Vigil</title>
<link href="../css/reset.css" rel="stylesheet" type="text/css">
</head>
<body style="font-family: Impact, Haettenschweiler, 'Franklin Gothic Bold', 'Arial Black', 'sans-serif'">
	<img src="central-park.jpg" width="700" height="378" alt="Central Park, NY"/>
<table style="background-color: purple; color: white;">
	<tr>
		<td colspan="2">PHOTO DETAILS</td>
		</tr>
		<tr>
			<td colspan="2">
	<form action="../scripts/process.php" method="post">
		<div><div>
			<label for="title">Titles</label></div>
		<input type="text" id="title" name="user_title">
		</div>
		<div><div>
			<label for="description">Description</label></div>
			<textarea id="description" name="user_description"></textarea>
		</div>
		</form>
			</td>
		</tr>
		<tr>
		<td><form action="../scripts/process.php" method="post">
		  <div><div>
			<label for="continet">Continet</label>
			  </div>
			<select name="user_continet">
			<option value="north_america">Norh America</option>
			<option value="south_america">South America</option>
			<option value="europe">Europe</option>
			<option value="asia">Asia</option>
			<option value="africa">Africa</option>
			<option value="australia">Australia</option>
			  </select>
			</div>
			<div><div>
			<label for="country">Country</label>
				</div>
				<select name="user_country">
					<option value="usa">USA</option>
					<option value="canada">Canada</option>
					<option value="mexico">Mexico</option>
					<option value="uk">UK</option>
					<option value="eu">EU</option>
					<option value="russia">Russia</option>
					<option value="brazil">Brazil</option>
					<option value="south_africa">South Africa</option>
					<option value="china">China</option>
					<option value="japan">Japan</option>
					<option value="south_korea">South Korea</option>
					<option value="india">India</option>
					<option value="australia">Australia</option>
				</select>
			</div><div>
			<label for="city">City</label>
			</div>
			<input type="text" id="city" name="user_city">
		</form></td>
			<td><form action="../scripts/process.php" method="post">
				<div><div>
					<label for="copyright">Copyright?</label>
					</div>
				<input type="radio" value="copyright" name="user_copyright">All rights reserved<br>
					<input type="radio" value="creative_commons">Creative Commons</div>
				<div><div>
				<label for="creative_common_type">Creative Commons Type</label>
					</div>
					<input type="checkbox" value="attribution">Attributtion<br>
					<input type="checkbox" value="noncommercial">Noncommercial<br>
					<input type="checkbox" value="derivative_work">No Derivative Works
				</div>
				</form></td>
		</tr>
		<tr>
		<td colspan="2">
			<form action="../scripts/process.php" method="post">
			<div>
				<label for="agreement">I accept the software licnese</label>
				<input type="checkbox" value="agreement">
				</div>
			</form>
			</td>
		</tr>
		<tr>
		<td>
			<form action="../scripts/process.php" method="post">
			<div><div>
				<label for="rating">Rate this photo</label>
				</div>
				<input type="number" name="rateing" min="0" max="10">
				</div>
				<div><div>
				<label for="color">Color Collection</label>
					</div>
					<input type="color" name="color_sel">
				</div>
			</form>
			</td>
		<td>
			<form action="../scripts/process.php" method="post">
			<div><div>
				<label for="date">Date Taken</label>
				</div>
				<input type="date" name="user_date">
				</div>
				<div><div>
				<label for="time">Time Taken</label>
					</div>
					<input type="time" name="user_time">
				</div>
			</form>
			</td>
		</tr>
		<tr>
		<td colspan="2">
			<button style="background-color: red; color: white">Submit</button>
			<button style="background-color: red; color: white">Clear form</button>
			</td>
		</tr>
	</table>
	
</body>
</html>